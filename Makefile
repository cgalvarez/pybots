init:
	@# Initializes the project.
	@#   Usage: `make init`
	@python3 -m venv .venv && \
	. .venv/bin/activate && \
	pip install --upgrade pip && \
	pip install -r requirements-dev.txt && \
	pip install -r requirements-prod.txt

pytest:
	@# Run all tests configured with PyTest.
	@#   Usage: `make pytest`
	@. .venv/bin/activate && \
	pytest

example:
	@# Run the example.
	@#   Usage: `make example`
	@. .venv/bin/activate && \
	python example.py
