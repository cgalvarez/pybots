# -*- coding: utf-8 -*-
"""Provide geolocation info for IP adresses."""
from glob import glob
from os.path import abspath, dirname, join
from pathlib import Path
import geoip2.database

from pybots.utils import ensure_path_exists


class GeoLocator():
    """Provide geolocation info from MaxMind GeoIP2 free databases."""

    # See https://dev.maxmind.com/geoip/geoip2/geolite2/
    _base_url = 'https://geolite.maxmind.com/download/geoip/database'
    _databases = ['City', 'Country', 'ASN']

    def __init__(self):
        """Init instance props."""
        self.asn = None
        self.city = None
        self._path = join(dirname(abspath(__file__)), '.tor', 'geolite2')
        ensure_path_exists(self._path)

    def read(self, **kwargs):
        """Read all available geodatabases.

        See https://github.com/maxmind/GeoIP2-python
        """
        cm = kwargs.pop('cm')
        root = join(cm.path, 'maxmind')
        ext = '.tar.gz'

        # Download missing databases.
        dl_kwargs = dict(checksums=True, decompress=root)
        for database in self._databases:
            filename = 'GeoLite2-' + database
            tarball = join(self._path, filename + ext)
            if not Path(tarball).is_file():
                url = '{}/{}{}'.format(self._base_url, filename, ext)
                print('DOWNLOADING {}'.format(url))
                cm.download(url, join(root, filename + ext), **dl_kwargs)

            pattern = join(root, filename + '_*')
            setattr(self, database.lower(), geoip2.database.Reader(
                join(glob(pattern)[0], filename + '.mmdb')))

    def close(self):
        """Close all open readers."""
        self.asn and self.asn.close()
        self.city and self.city.close()
