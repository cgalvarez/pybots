# -*- coding: utf-8 -*-
"""Some generic functions."""
from os import makedirs
from pathlib import Path
from typing import Union
import netifaces


def ensure_path_exists(dirpath: str):
    """Ensure a path exists."""
    if Path(dirpath).is_dir() is False:
        makedirs(dirpath)


def find_address(interface, family=netifaces.AF_INET):
    """Find the address of the requested `family` at the provided `interface`."""
    addresses = netifaces.ifaddresses(interface)
    return addresses[family][0]['addr']


def format_ipv4(address: str, port: Union[int, str] = None):
    """Format an IPv4 address (optionally with port) with right-align."""
    tokens = address.split('.')
    return '{:>3s}.{:>3s}.{:>3s}.{:>3s}'.format(tokens) if not port \
        else '{:>3s}.{:>3s}.{:>3s}.{:>3s}:{port:>5s}'.format(tokens, port=port)


