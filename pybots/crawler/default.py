# -*- coding: utf-8 -*-
"""Manager that handles a direct connection to the ISP."""
from pybots.crawler import BaseCrawler


class DefaultManager(BaseCrawler):
    """Connection manager that uses the default, public connection (public IP)."""

    def __init__(self, *args, **kwargs):
        """Init instance props."""
        self._path = kwargs.pop('path', '.bot-default')
        super().__init__(*args, **kwargs)
