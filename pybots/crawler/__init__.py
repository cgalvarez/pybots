# -*- coding: utf-8 -*-
"""Provide the base manager to be inherited/extended."""
from hashlib import sha512
from http import HTTPStatus
from os.path import dirname, join
from pathlib import Path
from pickle import dump, load, HIGHEST_PROTOCOL
from random import uniform
from time import sleep
from typing import List, Tuple
from user_agent import generate_user_agent
import netifaces
import requests
import shlex
import subprocess

from pybots.utils import ensure_path_exists, find_address


class BaseCrawler():
    """Provide common actions to any connection manager."""

    class Url:
        """Common URLs."""

        ICANHAZIP = 'https://icanhazip.com/'

    @property
    def path(self):
        """Return the private property value."""
        return self._path

    TYPE = None
    _cache_index = None  # type: dict
    _checksum_ext = ['.md5', '.sha']
    _signature_ext = ['.asc', '.sig']

    def __init__(self, *args, **kwargs):
        """Init instance props."""
        self._headers = None
        self._session = requests
        self._cache_path = join(self._path, 'cache')
        self._max_retries = kwargs.pop('max_retries', 10)
        self._wait = kwargs.pop('wait', 5)
        self._network = []
        self._init_layers()

    def _new_session(self):
        pass

    def _new_headers(self, **kwargs):
        """Autogenerate random headers for an HTTP request."""
        filtered = {k: v for k, v in kwargs.items()
                    if k in ['device', 'navigator', 'os', 'platform']}
        self._headers = {'User-Agent': generate_user_agent(**filtered)}
        return self._headers

    def _request(self, url: str, **kwargs):
        """Make an HTTP request through the configured network."""
        if kwargs.pop('randomize', False):
            self._new_headers()
            self._new_session()
        return self._session.get(url, headers=self._headers, **kwargs)

    def request(self, *args, **kwargs):
        """Make an HTTP request until successful or `max_retries` reached."""
        cache = kwargs.pop('cache', True)
        blob = kwargs.pop('blob', False)
        if not blob and cache:
            page = self._load_from_cache(args[0])
            if page:
                return page

        r, retries = None, 0
        abort_on_not_found = kwargs.pop('abort_on_not_found', True)
        while retries < self._max_retries:
            retries += 1
            print('TRY #{} ({}) FROM {}'.format(
                retries, args[0], self._request(self.Url.ICANHAZIP).text.strip()))  #, end='')
            r = self._request(*args, **kwargs)
            if r.status_code == HTTPStatus.OK or \
                    (abort_on_not_found and r.status_code == HTTPStatus.NOT_FOUND):
                break
            if retries == self._max_retries:
                return None
            delay = uniform(0.5 * self._wait, 1.5 * self._wait)
            if self.TYPE == 'tor':
                # TOR controller already provides its own waiting time.
                self.new_circuit()
            else:
                sleep(delay)

        if not blob and cache:
            self._save_into_cache(args[0], r.text)

        return r if blob else r.text

    def _load_from_cache(self, url: str):
        if url not in self._cache_index:
            return
        filepath = join(self._cache_path, self._cache_index[url] + '.bkp')
        try:
            with open(filepath, 'r') as f:
                return f.read()
        except:
            pass

    def _save_into_cache(self, url: str, contents: str):
        id = sha512(url.encode('utf-8')).hexdigest()
        self._cache_index[url] = id
        with open(join(self._cache_path, id + '.bkp'), 'w') as f:
            f.write(contents)

    def _save_cache_index(self):
        """Save the cache index dictionary into disk."""
        ensure_path_exists(self._cache_path)
        with open(join(self._cache_path, 'index.pkl'), 'wb') as f:
            dump(self._cache_index, f, HIGHEST_PROTOCOL)

    def _load_cache_index(self):
        """Load the cache index dictionary from disk."""
        try:
            with open(join(self._cache_path, 'index.pkl'), 'rb') as f:
                self._cache_index = load(f)
        except:  # noqa: E722
            self._cache_index = {}

    def _init_layers(self, **kwargs):
        """Configure the network layers (typically LAN, ISP, Tor)."""
        # LAN layer.
        interface = kwargs.get('interface', 'wlo1')
        self._add_layer(
            'LAN ({})'.format(interface),
            find_address(interface, family=netifaces.AF_INET),
            mac=find_address(interface, family=netifaces.AF_PACKET),
            ipv6=find_address(interface, family=netifaces.AF_INET6).split('%', 1)[0],
        )

        # ISP layer.
        # NOTE: Links with browser versions to fake user agent:
        #  - Chrome: https://en.wikipedia.org/wiki/Google_Chrome_version_history
        #  - Firefox: https://en.wikipedia.org/wiki/Firefox_version_history
        self._public_ip = self._request(self.Url.ICANHAZIP).text.strip()
        self._add_layer('ISP', self._public_ip)

        """
        # FIXME: Tor circuit.
        if not self._cm:
            self._cm = ConnectionManager(public_ip=isp_ip)
        self._add_layer('Tor', self.tor_request(self._cm.Url.ICANHAZIP).text.strip())

        return self._layers
        """

    def _add_layer(self, name: str, ipv4: str, **kwargs):
        """Add a network layer."""
        layer = dict(name=name, ipv4=ipv4)
        [kwargs.pop(k) for k in kwargs.keys() if k not in ['ipv6', 'mac']]
        layer.update(kwargs)
        self._network.append(layer)

    def download(self, url: str, target: str, **kwargs: dict):
        """Download the remote file at `url` into a local file `target`.

        This method is meant to be invoked for big files, not for common
        webpage requests, and cache is never used.
        Taken from: https://stackoverflow.com/questions/16694907/#answer-16696317
        """
        ensure_path_exists(dirname(target))
        pairs = [(url, target)]  # type: List[Tuple[str, str]]
        check_signatures = kwargs.pop('signatures', False)
        if check_signatures is True:
            pairs += [(url + ext, target + ext) for ext in self._signature_ext]
        check_checksums = kwargs.pop('checksums', False)
        if check_checksums is True:
            pairs += [(url + ext, target + ext) for ext in self._checksum_ext]

        r_kwargs = dict(stream=True, blob=True, cache=False)
        for i, (url, _target) in enumerate(pairs):
            if Path(_target).is_file():
                if i > 0:
                    continue
                else:
                    break
            with self.request(url, abort_on_not_found=i > 0, **r_kwargs) as r:

                if i == 0:
                    r.raise_for_status()
                elif r.status_code == HTTPStatus.NOT_FOUND:
                    continue  # Signatures/checksums may not exist...
                with open(_target, 'wb') as f:
                    for chunk in r.iter_content(chunk_size=8192):
                        if chunk:  # Filter out keep-alive new chunks
                            f.write(chunk)

        check_signatures and self._verify_signatures(target)
        check_checksums and self._verify_checksums(target)
        decompress_path = kwargs.pop('decompress', False)
        decompress_path and self._decompress(target, decompress_path)

    def _verify_signatures(self, target: str):
        """Verify GPG signatures (when available)."""
        for ext in self._signature_ext:
            if not Path(target + ext).is_file():
                continue
            cmd = shlex.split(
                'gpg --verify {target}{ext} {target}'.format(target=target, ext=ext))

            completed = subprocess.run(cmd, cwd='.')
            if completed.returncode != 0:
                raise Exception('{}\nSignature verification failed for {}'.format(
                    completed.stderr.decode('ascii'), target))

    def _verify_checksums(self, target: str):
        """Verify checksums (when available)."""
        for ext in self._checksum_ext:
            if not Path(target + ext).is_file():
                continue

            failed, cmd = 0, None
            if ext == '.md5':
                with open(target, 'rb') as f, open(target + ext, 'r') as s:
                    from hashlib import md5
                    failed = md5(f.read()).hexdigest() != s.read()
            else:
                # TODO Not yet implemented.
                continue

            if cmd:
                completed = subprocess.run(cmd, cwd='.', capture_output=True)
                failed = completed.returncode != 0

            if failed != 0:
                raise Exception('{}\nChecksum verification failed for {}'.format(
                    completed.stderr.decode('utf-8'), target))

    def _decompress(self, input, output):
        """Decompress a tarball file."""
        import tarfile
        tar = tarfile.open(input, 'r:*')
        tar.extractall(path=output)
