# -*- coding: utf-8 -*-
"""Manager that handles a connection through TOR network.

Tutorial: https://jarroba.com/anonymous-scraping-by-tor-network/
Sources: https://github.com/RicardoMoya/Scraping_Proxy_Tor

See Also
--------
- https://blog.jessfraz.com/post/routing-traffic-through-tor-docker-container/
- https://www.scrapehero.com/tutorial-how-to-scrape-amazon-product-details-using-python/

"""
from functools import partial
from random import randint, SystemRandom
from stem import Signal, StreamStatus
from stem.control import EventType, Controller
from time import sleep
import docker
import requests
import stem
import string
import sys

from pybots.crawler import BaseCrawler
from pybots.geo import GeoLocator


class TorManager(BaseCrawler):
    """Manager that handles connections to Tor network."""

    TYPE = 'tor'
    _container_name = 'pybots_tor_client'
    _pw_chars = string.ascii_uppercase + string.digits

    def __init__(self, *args, **kwargs):
        """Init instance props."""
        self._path = kwargs.pop('path', '.bot-tor')

        super().__init__(*args, **kwargs)

        # Props to interact with TOR.
        self._control_port = int(kwargs.pop('control_port', 0))  # 9051
        self._socks_port = int(kwargs.pop('socks_port', 0))  # 9050
        self._password = None
        self._tor_controller = None
        self._tor_proxies = None
        self._tor_version = None

        self._new_ip = self._public_ip
        self._old_ip = None
        self._address = kwargs.pop('address', '0.0.0.0')  # 127.0.0.1')
        self._timeout = kwargs.pop('timeout', 30)
        self._cb_stream_event = kwargs.pop('cb_stream_event', None)
        self._geolocate = kwargs.pop('geolocate')

        # Next ones are set by methods.
        self._docker = docker.from_env()
        self._geolocator = GeoLocator()
        self._stream_listener = None

    def __enter__(self):
        """Autogenerate HTTP session/headers, connect to TOR and read geodatabases."""
        while not self._socks_port or not self._is_port_closed(self._socks_port):
            self._socks_port = randint(10000, 60000)
        while not self._control_port or not self._is_port_closed(self._control_port):
            self._control_port = randint(10000, 60000)
        socks_address = 'socks5://{}:{}'.format(self._address, self._socks_port)
        self._tor_proxies = dict(http=socks_address, https=socks_address)
        self._password = ''.join(SystemRandom().choice(self._pw_chars)
                                 for _ in range(1024))

        # Stop and remove docker container if already running.
        self._stop_container()
        self._docker.containers.run('andreas4all/tor-client:latest',
                                    detach=True,
                                    environment=dict(PASSWORD=self._password),
                                    name=self._container_name,
                                    ports={
                                        '9050/tcp': self._socks_port,
                                        '9051/tcp': self._control_port,
                                    })
        self._connect()
        self._new_headers()
        self._new_session()
        self._load_cache_index()
        if self._geolocate is True:
            self._geolocator.read(cm=self)
        return self

    def __exit__(self, exception_type, exception_value, traceback):
        """Close geodatabase readers and close TOR controller."""
        if self._geolocate is True:
            self._geolocator.close()

        self._tor_controller.close()
        self._save_cache_index()
        self._stop_container()

        if exception_value:
            raise exception_value

    def _stop_container(self):
        try:
            container = self._docker.containers.get(self._container_name)
            container.stop()
            container.remove(force=True)
        except:  # noqa: E722
            pass


    def _is_port_closed(self, port: int, host: str = '127.0.0.1'):
        import socket
        from contextlib import closing
        with closing(socket.socket(socket.AF_INET, socket.SOCK_STREAM)) as sock:
            sock.settimeout(2)
            return sock.connect_ex((host, port)) != 0

    def new_circuit(self, **kwargs):
        """Establish a new TOR circuit so the exit IP changes."""
        # If we get the same IP, we'll wait some seconds to check again.
        seg = 0
        self._old_ip = self._new_ip
        while self._old_ip == self._new_ip and seg < self._timeout:
            delay = self._tor_controller.get_newnym_wait()
            seg += delay
            sleep(delay)
            self._tor_controller.signal(Signal.NEWNYM)
            self._new_ip = self._request(self.Url.ICANHAZIP, randomize=True).text.strip()

        if self._old_ip == self._new_ip:
            raise Exception('Public IP leaked! ' +
                            'TOR exit IP is the same as your ISP public IP!!')

    def _connect(self):
        """Connect to TOR network by creating our own controller."""
        if not self._tor_controller:
            try:
                self._tor_controller = Controller.from_port(
                    address=self._address, port=self._control_port)
            except stem.SocketError as e:
                print('Unable to connect to TOR on port {}: {}'.format(
                    self._socks_port, e))
                sys.exit(1)
            self._stream_listener = partial(self._on_stream_event, self._tor_controller)
            self._tor_controller.add_event_listener(self._stream_listener,
                                                    EventType.STREAM)

        e = None
        for i in range(5):
            if self._tor_controller.is_authenticated():
                break
            try:
                self._tor_controller.authenticate(password=self._password)
                authenticated = True
            except stem.connection.AuthenticationFailure as e:
                pass
            sleep(.5)
        if not self._tor_controller.is_authenticated():
            print('Unable to authenticate: {}'.format(e))
            sys.exit(1)

        if not self._tor_version:
            self._tor_version = self._tor_controller.get_version()

        self._tor_controller.signal(Signal.NEWNYM)

    def _on_stream_event(self, controller, event):
        """Provide info about TOR circuit (ID and relays).

        Invoked when a stream event is fired.
        """
        if not self._cb_stream_event \
                or event.status != StreamStatus.SUCCEEDED \
                or not event.circ_id:
            return

        try:
            circuit = self._tor_controller.get_circuit(event.circ_id)
        except e as ValueError:
            pass

        try:
            if len(circuit.path) != 3:
                return

            info = dict(id=circuit.id, target=event.target, relays=[])
            for path in circuit.path:
                fingerprint = path[0]
                relay = self._tor_controller.get_network_status(fingerprint, None)
                if relay is None:
                    continue
                relay_info = dict(
                    nickname=relay.nickname,
                    fingerprint=relay.fingerprint,
                    address=relay.address,
                    port=relay.or_port,
                    country=None,
                    tor_country=self._tor_controller.get_info(
                        'ip-to-country/' + relay.address, 'unknown'),
                )
                if self._geolocate and self._geolocator.city:
                    reader = self._geolocator.city
                    city = reader.city(relay.address)
                    relay_info['country'] = '{} ({})'.format(city.country.name, city.country.iso_code)
                info['relays'].append(relay_info)
        except Exception as e:
            import traceback
            traceback.print_exc()

        if len(info['relays']) != 3:
            return

        self._cb_stream_event(info)

    def _new_session(self):
        """Create a new requests session."""
        self._session = requests.session()
        self._session.proxies = self._tor_proxies
