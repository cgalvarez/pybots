from bs4 import BeautifulSoup
from pybots.crawler.default import DefaultManager
from pybots.crawler.tor import TorManager


def print_details(info):
    """Print the info about the used TOR circuit."""
    print('{}'.format(info))


def main():
    url = 'https://icanhazip.com'

    isp_cm = DefaultManager()
    ip = isp_cm.request(url, cache=False)
    print('ISP IP:', ip)

    tor_kwargs = dict(
        max_retries=20,
        wait=5,
        geolocate=False,
        cb_stream_event=print_details,
    )
    with TorManager(**tor_kwargs) as cm:
        ip = cm.request(url)
        print('TOR IP:', ip)

        html = cm.request('https://stackoverflow.com')
        if html:
            print("TODAY'S TOP STACKOVERFLOW QUESTIONS:")
            parsed = BeautifulSoup(html, 'html.parser')
            container = parsed.find('div', {'id': 'qlist-wrapper'})
            for link in container.find_all('a', 'question-hyperlink'):
                print('  - {}'.format(link.contents[0]))


if __name__ == '__main__':
    main()
